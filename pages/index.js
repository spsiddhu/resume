import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Siddarthan Sarumathi Pandian</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
 
        <div className={styles.firstLine}>
          Hello! I am Siddarthan Sarumathi Pandian, thanks for stopping by. I have been working as programmer since 2014.
        </div>

        <br/>
        <div className={styles.secondLine}>
          I currently work at <span className={styles.underline}><a href="https://automattic.com/">Automattic</a></span> as a backend engineer. 
        </div>



        <div className={styles.grid}>
          <a href="https://www.linkedin.com/in/siddarthan-sarumathi-pandian-a3aa9943/" className={styles.card}>
            <h3>Linkedin</h3>
            <p>Work experience</p>
          </a>

          <a href="https://medium.com/@siddarthansp" className={styles.card}>
            <h3>Blog</h3>
            <p>I blog here once in a bluemoon</p>
          </a>

          <a
            href="https://github.com/spsiddarthan"
            className={styles.card}
          >
            <h3>Github</h3>
            <p>Where I push code </p>
          </a>

          <a
            href="/resume.pdf" download
            className={styles.card}
          >
            <h3>Resume</h3>
            <p>
              Click to download
            </p>
          </a>
        </div>
      </main>

      <div className={styles.secondLine}>
        My email is siddarthan@outlook.com should you want to contact me!
      </div>


    </div>
  )
}
